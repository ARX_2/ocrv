<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Claim extends Model
{
  protected $fillable = [
          'description',
          'status',
      ];
  public function comments(){
    return $this->HasMany('App\Comment');
  }

  static function status($statusid){
    $status[1] = 'Новая';
    $status[2] = 'В работе';
    $status[3] = 'Решена';
    $status[4] = 'Отклонена';
    return $status[$statusid];
  }
}
