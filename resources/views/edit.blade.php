<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>

            <div class="content">
              <form action="{{route('claim.update', $claim->id)}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
                <input type="text" name="description" value="{{$claim->description}}" placeholder="Описаие">
                <select  name="status">
                  <option value="1" @if($claim->status == 1) selected @endif>Новая</option>
                  <option value="2" @if($claim->status == 2) selected @endif>В работе</option>
                  <option value="3" @if($claim->status == 3) selected @endif>Решена</option>
                  <option value="4" @if($claim->status == 4) selected @endif>Отклонена</option>
                </select>
                <button type="submit" name="button">Создать</button>
              </form>
            </div>

    </body>
</html>
