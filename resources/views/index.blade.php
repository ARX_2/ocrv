<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>

            <div class="content">
              <a href="{{route('claim.create')}}">Создать заявку</a>
              <table>
                <thead>
                  <tr>
                    <th>№ Заявки</th>
                    <th>Описание</th>
                    <th>Статус</th>
                    <th>Комментарии</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($claims as $claim)
                  <tr>
                    <td>{{$claim->id}}</td>
                    <td>{!! $claim->description !!}</td>
                    <td>{{Claim::status($claim->status)}}</td>
                    <td><a href="{{route('claim.edit', $claim->id)}}">Редактировать</a> </td>
                    <td>
                      <form  action="{{route('claim.destroy', [$claim->id])}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" name="button">Удалить</button>
                      </form>
                     </td>
                    <td>

                      @forelse($claim->comments as $comment)
                      <li>id: {{$comment->id}}</li>
                      <li>Оп: {!! $comment->description !!}</li>
                      @empty
                      @endforelse
                      <form action="{{route('comment.store')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="claim_id" value="{{$claim->id}}">
                        <input type="text" name="description" placeholder="Описание">
                        <button type="submit" name="button">Отправить</button>
                      </form>
                    </td>
                  </tr>
                  @empty
                  @endforelse
                </tbody>
              </table>
            </div>

    </body>
</html>
